﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PathFinderAlgorithm{
	BreadthFirts = 0,
	DepthFirts,
	Dijkstra,
	AStar
}

public enum IAMode {
	Default = 0,
    FsmWithoutPathFinder,
	FsmWithPathFinder,
	BehaviourTree
}

namespace Manager{
    public class PathFinderManager : MonoBehaviour {

        [SerializeField]
        private GameObject _uiDebug = null;

		private PathFinder.PathFinder _pathFinder;

		public int _sizeX = 7;

		public int _sizeY = 7;

		public PathFinderAlgorithm _algorithm;

		public IAMode _mode;

        public bool _generateMap;

        public bool debug;

        public PathFinder.PathFinder.FindMode _findMode = PathFinder.PathFinder.FindMode.WithoutDiagonals;

		private void Awake(){
			GoldMiner.Worker._sIAMode = _mode;

            if (!_generateMap)
                return;

            switch (_algorithm) {
                case PathFinderAlgorithm.BreadthFirts:
                    _pathFinder = new PathFinder.BreadthFirst();
                    break;
                case PathFinderAlgorithm.DepthFirts:
                    _pathFinder = new PathFinder.DepthFirst();
                    break;
                case PathFinderAlgorithm.Dijkstra:
                    _pathFinder = new PathFinder.Dijkstra();
                    break;
                case PathFinderAlgorithm.AStar:
                    _pathFinder = new PathFinder.AStar();
                    break;
            }

            _pathFinder.debug = debug;

			_pathFinder.GenerateGrid (transform, _sizeY, _sizeX);

			CreateBase (2, 1);

			CreateWorker (0, 0);
			//CreateWorker (2, 2);
			//CreateWorker (2, 2);


			SetObstacleAt (3, 3);
			SetObstacleAt (4, 4);
			SetObstacleAt (6, 4);

			CreateMine (5, 5, Color.grey);

			CreateMine (0, 6, Color.gray);

            _pathFinder.findMode = _findMode;

            _pathFinder.pathFinderAlgorithm = _algorithm;

            _pathFinder.GetAdjacent();

            _uiDebug.SetActive(_pathFinder is PathFinder.Dijkstra || _pathFinder is PathFinder.AStar);
		}

		private void SetObstacleAt(int x, int z){
			if (_pathFinder.IsInGridRange (x, z)) {
				PathFinder.Node n = _pathFinder.GetNodeAt (x, z);

				if (n == null) {
					Debug.LogError ("Triying to get a null position");
					return;
				}

				n.color = Color.black;
				n.isObstacle = true;
				Vector3 v3 = n.transform.localScale;
				v3.y = 0.7f;
				n.transform.localScale = v3;
				n.transform.position = new Vector3 (n.transform.position.x, (v3.y / 2.0f) - 0.15f, n.transform.position.z);
			}
		}

		private void CreateMine(int x, int z, Color c){
			if (_pathFinder.IsInGridRange (x, z)) {
				PathFinder.Node n = _pathFinder.GetNodeAt (x, z);
                //n.isObstacle = true;

				GameObject mine = GameObject.Instantiate<GameObject> (PrefabManager.GetInstance()._mineGold);
				mine.GetComponent<MeshRenderer> ().material.color = c;

				mine.transform.position = new Vector3 (n.transform.position.x, mine.transform.position.y, n.transform.position.z);
				mine.transform.SetParent (this.transform);
			}
		}

		private void CreateBase(int x, int z){
			if (_pathFinder.IsInGridRange (x, z)) { 
				PathFinder.Node n = _pathFinder.GetNodeAt (x, z);
				//n.isObstacle = true;

				GameObject ba = GameObject.Instantiate<GameObject> (PrefabManager.GetInstance()._base);
				ba.transform.position = new Vector3 (n.transform.position.x, ba.transform.position.y, n.transform.position.z);
				ba.transform.SetParent (this.transform);
			}
		}

		private void CreateWorker(int x,int z){
			if (_pathFinder.IsInGridRange (x, z)) {
				PathFinder.Node n = this._pathFinder.GetNodeAt (x, z);

				GameObject go = GameObject.Instantiate<GameObject> (PrefabManager.GetInstance()._worker);
				go.transform.position = new Vector3 (n.transform.position.x, go.transform.position.y, n.transform.position.z);

				go.transform.SetParent (this.transform);
			}
		}
			
	}

}