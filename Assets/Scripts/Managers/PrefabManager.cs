﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager{
	public class PrefabManager : MonoBehaviour {

		private static PrefabManager _sInstance = null;

		public GameObject _nodePrefab = null;

		public GameObject _mineGold = null;

		public GameObject _base = null;

		public GameObject _worker = null;

		private void Awake(){
			_sInstance = this;
		}

		public static PrefabManager GetInstance(){
			return _sInstance;
		}
	}
}