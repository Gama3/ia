﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Events{
	OnIdle,
	OnGoToMine,
	OnCollectingResources,
	OnReturningHome,
	OnLeaveStuff
}

public enum States{
	Idle,
	GoToMine,
	CollectStuff,
	ReturnHome,
	LeaveStuff
}

public class IsThereAMine : BehaviourTree.ConditionalNode{
	private GoldMiner.Worker _worker;

	public IsThereAMine(GoldMiner.Worker worker){
		this._worker = worker;
	}

	public override BehaviourTree.Status Execute (){
		RaycastHit workerHit;
		RaycastHit goldMineHit;
		PathFinder.Node goldMineNode = null;

		if (_worker.node == null) {
			if (Physics.Raycast (this._worker.transform.position, Vector3.down, out workerHit))
				this._worker.node = workerHit.collider.GetComponent<PathFinder.Node> ();
		}
		if (goldMineNode == null && _worker.nearestResource == null) {
			if ((_worker.nearestResource = this._worker.FindClosestGameObjects (GameObject.FindGameObjectsWithTag ("Resource"))) != null) {
				if (Physics.Raycast (_worker.nearestResource.transform.position, Vector3.down, out goldMineHit)) {
					goldMineNode = goldMineHit.collider.GetComponent<PathFinder.Node> ();
					_worker.goldMine = _worker.nearestResource.GetComponent<GoldMiner.GoldMine> ();

					return _worker.goldMine.currentGold <= 0 ? BehaviourTree.Status.Fail : BehaviourTree.Status.Success;
				}
			}
		}
		else
			return _worker.goldMine.currentGold <= 0 ? BehaviourTree.Status.Fail : BehaviourTree.Status.Success;
		return BehaviourTree.Status.Fail;
	}
}

public class Idle : BehaviourTree.ActionNode{

	private GoldMiner.Worker _worker;

	public Idle(GoldMiner.Worker worker){
		this._worker = worker;
	}

	public override BehaviourTree.Status Execute (){
		return this._worker.goldMine.currentGold > 0 ? BehaviourTree.Status.Success : BehaviourTree.Status.Fail;
	}
}

public class GoToMine : BehaviourTree.ActionNode{
	private GoldMiner.Worker _worker;

	public GoToMine(GoldMiner.Worker worker){
		_worker = worker;
	}

	public override BehaviourTree.Status Execute (){
        //Optimizar el path de ida(base hasta mina)  se esta calculando cada 
        //if (_worker._toPath == null) {
            _worker._toPath = PathFinder.PathFinder.GetInstance().GetPath(_worker.node, _worker.goldMine._myNode);
        //}

		Vector3 currentPosition = this._worker.transform.position;

		Vector3 pos = this._worker._toPath[this._worker.currentWaypoint].transform.position;
		pos.x -= 0.5f;
		pos.z -= 0.5f;

		Vector3 targetPosition = this._worker.currentWaypoint == this._worker._toPath.Count - 1
			? pos 
			: this._worker._toPath [this._worker.currentWaypoint].transform.position;

		if (Vector3.Distance (currentPosition, targetPosition) > 0.7f) {
			Vector3 directionOfTravel = targetPosition - currentPosition;
			directionOfTravel.Normalize ();

			this._worker.transform.Translate (
				directionOfTravel.x * Time.deltaTime * this._worker.velocity,
				0.0f,
				directionOfTravel.z * Time.deltaTime * this._worker.velocity,
				Space.World
			);
		}
		else {
			if (this._worker.currentWaypoint + 1 < this._worker._toPath.Count)
				++this._worker.currentWaypoint;
			else { //llegue a la mina
				_worker.ResetPath ();
				return BehaviourTree.Status.Success;
			}
		}
		return BehaviourTree.Status.InProcess;
	}
}

public class CollectingResources : BehaviourTree.ActionNode{

	private GoldMiner.Worker _workerRef;

	public CollectingResources(GoldMiner.Worker worker) : base(){
		this._workerRef = worker;
	}

	~CollectingResources(){}

	public override BehaviourTree.Status Execute () {
		if (this._workerRef.amountOfResource < GoldMiner.Worker.MAX_AMOUNT_OF_RESOURCES && this._workerRef.goldMine.currentGold > 0) {
			this._workerRef.timeMining += Time.deltaTime * 5.0f; //eliminar el 5
			if (this._workerRef.timeMining > GoldMiner.Worker.TIME_TO_GET_A_UNIT_OF_GOLD) {
				++this._workerRef.amountOfResource;
				--this._workerRef.goldMine.currentGold;
				this._workerRef.timeMining = 0.0f;
				return BehaviourTree.Status.InProcess;
			}
		}
		else
			return BehaviourTree.Status.Success;
		return BehaviourTree.Status.InProcess;
	}
}

public class ReturnToHome : BehaviourTree.ActionNode{
	private GoldMiner.Worker _workerRef;

	public ReturnToHome(GoldMiner.Worker worker){
		this._workerRef = worker;
	}

	~ReturnToHome(){}

	public override BehaviourTree.Status Execute () {
		RaycastHit workerHit;
		if (Physics.Raycast (_workerRef.transform.position, Vector3.down, out workerHit))
			_workerRef.node = workerHit.collider.GetComponent<PathFinder.Node> ();

		_workerRef._fromPath = PathFinder.PathFinder.GetInstance ().GetPath (this._workerRef.node, this._workerRef.GetNode (this._workerRef.basee.GetNode ()));

        if (this._workerRef._fromPath == null) {
            Debug.Log("wtf es null el path");
            return BehaviourTree.Status.Fail;
        }

		Vector3 currentPosition = _workerRef.transform.position;

        if (_workerRef.currentWaypoint + 1 > _workerRef._fromPath.Count)
            return BehaviourTree.Status.Success;

        Vector3 targetPosition = _workerRef._fromPath[_workerRef.currentWaypoint].transform.position;


		if (Vector3.Distance (currentPosition, targetPosition) > 0.7f) {

			Vector3 directionOfTravel = targetPosition - currentPosition;
			directionOfTravel.Normalize ();

			this._workerRef.transform.Translate (
				directionOfTravel.x * Time.deltaTime * this._workerRef.velocity,
				0.0f,
				directionOfTravel.z * Time.deltaTime * this._workerRef.velocity,
				Space.World
			);
		}
		else {
			if (this._workerRef.currentWaypoint + 1 < this._workerRef._fromPath.Count)
				++this._workerRef.currentWaypoint;
			else {
				this._workerRef.ResetPath ();
				return BehaviourTree.Status.Success;
			}

		}
		return BehaviourTree.Status.InProcess;
	}
}


public class LeaveStuff : BehaviourTree.ActionNode{
	private GoldMiner.Worker _worker;

	public LeaveStuff(GoldMiner.Worker worker){
		this._worker = worker;
	}

	public override BehaviourTree.Status Execute (){
		GoldMiner.Base._sGold +=  this._worker.amountOfResource;

		this._worker.amountOfResource = 0;

		return BehaviourTree.Status.Success;
	}
}

namespace GoldMiner{
	public class Worker : MonoBehaviour {
        public static IAMode _sIAMode = IAMode.Default;

		private BehaviourTree.SelectorNode _root;
		private Fsm<States,Events> _workerFsm;
		private GameObject _nearestResource;
		private GoldMine _goldMine;

		private Base _base;

		private int _amountOfResourceObtain;
		public const int MAX_AMOUNT_OF_RESOURCES = 10;

		private float _timeMining;
		public const float TIME_TO_GET_A_UNIT_OF_GOLD = 2.0f;

		private int _currentWaypoint;

		private float _velocity;

		private PathFinder.Node _node;

        public List<PathFinder.Node> _fromPath;
        public List<PathFinder.Node> _toPath;

		private void Start () {
			_base = GameObject.FindGameObjectWithTag ("Base").GetComponent<Base> ();

			_nearestResource = null;
			_goldMine = null;

			_amountOfResourceObtain = 0;

			_timeMining = 0.0f;

			_node = null;

			_currentWaypoint = 0;
			_velocity = 5.0f;

            _fromPath = null;
            _toPath = null;

			if (_sIAMode == IAMode.FsmWithPathFinder || _sIAMode == IAMode.FsmWithoutPathFinder) {
                _workerFsm = new Fsm<States, Events>(5, 5);

				_workerFsm.SetRelation (States.Idle, Events.OnGoToMine, States.GoToMine);

				_workerFsm.SetRelation (States.GoToMine, Events.OnCollectingResources, States.CollectStuff);

				_workerFsm.SetRelation (States.CollectStuff, Events.OnReturningHome, States.ReturnHome);

				_workerFsm.SetRelation (States.ReturnHome, Events.OnLeaveStuff, States.LeaveStuff);

				_workerFsm.SetRelation (States.LeaveStuff, Events.OnIdle, States.Idle);
				_workerFsm.SetRelation (States.LeaveStuff, Events.OnGoToMine, States.GoToMine);

				_workerFsm.SetEvent (Events.OnIdle);
			}
			else if (_sIAMode == IAMode.BehaviourTree) {
                _root = new BehaviourTree.SelectorNode(false);

                BehaviourTree.SequenceNode sn1 = new BehaviourTree.SequenceNode(true);

                sn1.AddChild(new IsThereAMine(this));
                sn1.AddChild(new Idle(this));
                sn1.AddChild(new GoToMine(this));

                sn1.AddChild(new CollectingResources(this));
                sn1.AddChild(new ReturnToHome(this));
                sn1.AddChild(new LeaveStuff(this));

                _root.AddChild(sn1);
			}
		}

        private void Update() {
            switch (Worker._sIAMode) {
                case IAMode.FsmWithPathFinder:

#if UNITY_EDITOR || UNITY_EDITOR_64
                    Debug.Log("Current State " + _workerFsm.GetState().ToString());
#endif //NITY_EDITOR || UNITY_EDITOR_64

                    switch (this._workerFsm.GetState()) {
                        case States.Idle:
                            Idle();
                            break;
                        case States.GoToMine:
                            Mine();
                            break;
                        case States.CollectStuff:
                            CollectingResources();
                            break;
                        case States.ReturnHome:
                            ReturnToHome();
                            break;
                        case States.LeaveStuff:
                            LeaveStuff();
                            break;
                    }
                    break;

                case IAMode.FsmWithoutPathFinder:
#if UNITY_EDITOR || UNITY_EDITOR_64
                    Debug.Log("Current State " + this._workerFsm.GetState().ToString());
#endif //NITY_EDITOR || UNITY_EDITOR_64

                    switch (this._workerFsm.GetState()) {
                        case States.Idle:
                            Idle2();
                            break;
                        case States.GoToMine:
                            Mine2();
                            break;
                        case States.CollectStuff:
                            CollectingResources();
                            break;
                        case States.ReturnHome:
                            ReturnToHome2();
                            break;
                        case States.LeaveStuff:
                            LeaveStuff();
                            break;
                    }
                    break;

                case IAMode.BehaviourTree:
                     Debug.Log(_root.Execute());
                    break;
            }
        }

		private void Idle(){
			RaycastHit workerHit;
			RaycastHit goldMineHit;
			PathFinder.Node goldMineNode = null;

			if (_node == null) {
				if (Physics.Raycast (this.transform.position, Vector3.down, out workerHit))
					_node = workerHit.collider.GetComponent<PathFinder.Node> ();
			}
			if (goldMineNode == null && _nearestResource == null) {
				if ((_nearestResource = FindClosestGameObjects (GameObject.FindGameObjectsWithTag ("Resource"))) != null) {
					if (Physics.Raycast (_nearestResource.transform.position, Vector3.down, out goldMineHit)) {
						goldMineNode = goldMineHit.collider.GetComponent<PathFinder.Node> ();
						_goldMine = _nearestResource.GetComponent<GoldMine> ();
					}
				}
			}
			if (_node != null && goldMineNode != null && this._goldMine != null) {
               // if (_toPath == null)
                    _toPath = PathFinder.PathFinder.GetInstance().GetPath(_node, goldMineNode);
			}
			if (_toPath != null && _nearestResource != null)
				_workerFsm.SetEvent (Events.OnGoToMine);
		}

        /// <summary>
        /// without pathfinder
        /// </summary>
        private void Idle2() {
            if (_nearestResource == null) {
                if ((_nearestResource = FindClosestGameObjects(GameObject.FindGameObjectsWithTag("Resource"))) != null) 
                    _goldMine = _nearestResource.GetComponent<GoldMine>();
            }

            if (_nearestResource != null)
                _workerFsm.SetEvent(Events.OnGoToMine);
        }

		private void Mine(){
			Vector3 currentPosition = transform.position;

			Vector3 pos = _toPath[_currentWaypoint].transform.position;
			pos.x -= 0.5f;
			pos.z -= 0.5f;

			Vector3 targetPosition = _currentWaypoint == _toPath.Count - 1 ? pos : _toPath[_currentWaypoint].transform.position;

			if (Vector3.Distance (currentPosition, targetPosition) > 0.7f) {
				Vector3 directionOfTravel = targetPosition - currentPosition;
				directionOfTravel.Normalize ();

				transform.Translate (
					directionOfTravel.x * Time.deltaTime * _velocity,
					0.0f,
					directionOfTravel.z * Time.deltaTime * _velocity,
					Space.World
				);
			}
			else {
				if (_currentWaypoint + 1 < _toPath.Count)
					++_currentWaypoint;
				else { //llegue a la mina
					ResetPath ();
					_workerFsm.SetEvent (Events.OnCollectingResources);
				}

			}
		}

        /// <summary>
        /// Without pathFinder
        /// </summary>
        private void Mine2() {
            Vector3 currentPosition = transform.position;

            Vector3 targetPosition = _goldMine.transform.position;

            if (Vector3.Distance(currentPosition, targetPosition) > 0.7f) {
                currentPosition = Vector3.MoveTowards(currentPosition, targetPosition, Time.deltaTime * this.velocity);

                transform.position = currentPosition;
            }
            else { //llegue a la mina
                ResetPath();
                _workerFsm.SetEvent(Events.OnCollectingResources);
            }
        }

        /// <summary>
        /// do the same thing withPathFinder/withOutPathFinder
        /// </summary>
		private void CollectingResources(){
			if (_amountOfResourceObtain < MAX_AMOUNT_OF_RESOURCES && _goldMine.currentGold > 0) {
				_timeMining += Time.deltaTime * 5.0f; //eliminar el 5
				if (_timeMining > TIME_TO_GET_A_UNIT_OF_GOLD) {
					++_amountOfResourceObtain;
					--_goldMine.currentGold;
					_timeMining = 0.0f;
				}
			}
			else 
				_workerFsm.SetEvent (Events.OnReturningHome);
		}

        private void ReturnToHome() {
            if(_node == null) {
                RaycastHit workerHit;
                if(Physics.Raycast(transform.position, Vector3.down, out workerHit))
                    _node = workerHit.collider.GetComponent<PathFinder.Node>();
            }
             else
                _fromPath = PathFinder.PathFinder.GetInstance().GetPath(_node, GetNode(_base.GetNode()));

            if (_fromPath == null) {
                //Debug.LogError("Return path NULL");
               //error aqui cuando hay 8 direcciones(con diagonales) devuelve null!, en los 4 algoritmos
                return;
            }

            Vector3 currentPosition = transform.position;

            Vector3 targetPosition = _fromPath[_currentWaypoint].transform.position;

            //Debug.Log (this._path.Count);

            if(Vector3.Distance(currentPosition, targetPosition) > 0.7f) {

                Vector3 directionOfTravel = targetPosition - currentPosition;
                directionOfTravel.Normalize();

                transform.Translate(
                    directionOfTravel.x * Time.deltaTime * _velocity,
                    0.0f,
                    directionOfTravel.z * Time.deltaTime * _velocity,
                    Space.World
                );
            }
            else {
                if (_currentWaypoint + 1 < _fromPath.Count)
                    ++_currentWaypoint;
                else {
                    ResetPath();
                    _workerFsm.SetEvent(Events.OnLeaveStuff);
                }

            }
        }

        /// <summary>
        /// Without pathFinder
        /// </summary>
        private void ReturnToHome2() {
            Vector3 currentPosition = this.transform.position;

            Vector3 targetPosition = _base.transform.position;

            if (Vector3.Distance(currentPosition, targetPosition) > 0.7f) {

                currentPosition = Vector3.MoveTowards(currentPosition, targetPosition, Time.deltaTime * this._velocity);

                this.transform.position = currentPosition;
            }
            else {
                ResetPath();
                _workerFsm.SetEvent(Events.OnLeaveStuff);
            }
        }

        /// <summary>
        /// Do the same thing withPathFinder/withOutPathFinder
        /// </summary>
		private void LeaveStuff() {
            Base.SetGold(Base.GetGold() + _amountOfResourceObtain);
            _amountOfResourceObtain = 0;
            _workerFsm.SetEvent(Events.OnIdle);
            _nearestResource = null;
        }

		public PathFinder.Node GetNode(PathFinder.Node node){
			for(int i = 0; i < node.adjacent.Count; ++i){
				if (!node.adjacent [i].isObstacle)
					return node.adjacent [i];
			}
			return node;
		}

		public void ResetPath(){
			_node = null;
			//_path = null;
			_currentWaypoint = 0;
		}

		public GameObject FindClosestGameObjects(GameObject[] gos) {
			GameObject closest = null;
			float distance = Mathf.Infinity;
			foreach (GameObject go in gos) {
				Vector3 diff = go.transform.position - this.transform.position;
				float curDistance = diff.sqrMagnitude;
				if (curDistance < distance) {
					closest = go;
					distance = curDistance;
				}
			}
			return closest;
		}

		public PathFinder.Node node{
			get{
				return _node;
			}
			set{
				_node = value;
			}
		}

		public GoldMine goldMine{
			get{
				return _goldMine;
			}
			set{
				_goldMine = value;
			}
		}

		public GameObject nearestResource {
			get {
				return _nearestResource;
			}
			set {
				_nearestResource = value;
			}
		}

		public List<PathFinder.Node> path{
			get{
				return null;
			}
			set{
				//_path = value;
			}
		}

		public int currentWaypoint{
			get{
				return _currentWaypoint;
			}
			set{
				_currentWaypoint = value;
			}
		}

		public float velocity{
			get {
				return _velocity;
			}
			set{
				_velocity = value;
			}
		}

		public int amountOfResource{
			get{
				return _amountOfResourceObtain;
			}
			set{
				_amountOfResourceObtain = value;
			}
		}

		public float timeMining{
			get{
				return _timeMining;
			}
			set{
				_timeMining = value;
			}
		}

		public Base basee{
			get{
				return _base;
			}
			set{
				_base = value;
			}
		}

	}
}