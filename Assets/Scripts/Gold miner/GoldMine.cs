﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GoldMiner{
	public class GoldMine : MonoBehaviour {
		[SerializeField]
		private TextMesh _currentGoldText = null;

		public PathFinder.Node _myNode;
		public int currentGold;

		private void Start(){
            this.currentGold = Random.Range(10, 20);
			InvokeRepeating ("UpdateText", 0.0f, 1.0f);

			RaycastHit hit;
			if (Physics.Raycast (this.transform.position, Vector3.down, out hit)) 
				_myNode = hit.collider.GetComponent<PathFinder.Node> ();
		}

		private void Update () {
			if (this.currentGold <= 0)
				Destroy (this.gameObject);
		}

		private void UpdateText(){
			_currentGoldText.text = "Remaining gold: " + this.currentGold.ToString ();
		}
	}
		
}