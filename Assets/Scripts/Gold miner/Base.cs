﻿using UnityEngine;
using System.Collections;

namespace GoldMiner{
	public class Base : MonoBehaviour {
		[SerializeField]
		private TextMesh _goldText = null;

		public static int _sGold = 0;

		private PathFinder.Node _myNode;

		private void Start(){
			RaycastHit hit;
			if (Physics.Raycast (this.transform.position, Vector3.down, out hit)) 
				_myNode = hit.collider.GetComponent<PathFinder.Node> ();
		}

		private void Update(){
			this._goldText.text = "Gold " + _sGold.ToString ();
		}

		public PathFinder.Node GetNode(){
			return this._myNode;
		}

		public static void SetGold(int gold){
			_sGold = gold;
		}

		public static int GetGold(){
			return _sGold;
		}
	}
}
