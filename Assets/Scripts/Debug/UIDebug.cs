﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DebugOnly{
	public class UIDebug : MonoBehaviour {

		private static UIDebug _sInstance = null;

		[SerializeField]
		private Text _posMatrixText = null;

		[SerializeField]
		private Text heuristicText = null;

		[SerializeField]
		private Text _costText = null;

		private void Awake(){
			_sInstance = this;
		}

		public static UIDebug GetInstance(){
			return _sInstance;
		}

		public void SetPosMatrixText(string text){
			this._posMatrixText.text = text;
		}

		public void SetCostText(string text){
			this._costText.text = text;
		}

		public void SetHeuristicText(string text){
			this.heuristicText.text = text;
		}

	}
}
