﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LunarLander : LunarLanderBase{
	
	protected override void OnThink(float dt){
		Vector3 diff = (platform.transform.position - this.transform.position).normalized;

		Vector3 speedNormalize = speed.normalized;

		Vector3 platformPos = platform.transform.position.normalized;

		inputs[0] = diff.x;
		inputs[1] = diff.y;
		inputs[2] = speedNormalize.x;
		inputs[3] = speedNormalize.y;
		inputs[4] = platformPos.x;
		inputs[5] = platformPos.y;
        
		float[] outputs = brain.Synapsis(inputs);

		ThrottleRight(dt, outputs[0]);
		ThrottleLeft(dt, outputs[1]);
		ThrottleUp(dt, outputs[2]);

		float distance = Vector3.Distance(platform.transform.position, this.transform.position);

       
        if (speed.y < 1.0f) 
            Fitness += 0.00001f;
        

		if(distance < 25.0f){
			Fitness += 0.0003f;

            if (speed.y < 1.0f)
                Fitness += 1.0f;

			if(speed.magnitude > 10.0f)
				Fitness += 0.1f;
		}
	}

	protected override void OnRestart(){
	}

	protected override void OnCrashed(){
        Fitness -= 1.0f;
        Fitness = Mathf.Clamp(Fitness, 0.0f, Fitness);
	}

	protected override void OnLanded(){
        if (speed.y < 5.0f)
            Fitness += 1.0f;

        Fitness += speed.sqrMagnitude;
	}


}
