﻿using System;
using System.Collections;
using System.Collections.Generic;

	/*public class Fsm<TState,TEvent>
		where TState : struct, IConvertible
		where TEvent : struct, IConvertible{

		private Dictionary<TState, Dictionary<TEvent, TState>> _matrix;

		private TState _currentState;

		public Fsm(){
			this._currentState = default(TState);
			this._matrix = new Dictionary<TState, Dictionary<TEvent, TState>>();
		}

		~Fsm() {}

		public void SetRelation(TState srcState, TEvent evt, TState dstState){
			if (!this._matrix.ContainsKey (srcState))
				this._matrix [srcState] = new Dictionary<TEvent, TState> ();
			this._matrix [srcState] [evt] = dstState;
		}

		public void SetEvent(TEvent evt){
			if (this._matrix.ContainsKey(this._currentState) && this._matrix[this._currentState].ContainsKey(evt))
				this._currentState = this._matrix[this._currentState][evt];
		}

		public TState GetState(){
			return this._currentState;
		}
	}*/
public class Fsm<TState, TEvent>
    where TState : struct, IConvertible
    where TEvent : struct, IConvertible {

    private int[,] _fsm;

    private TState _currentState;

    private Fsm() {}

    public Fsm(int statesCount, int eventsCount) {
        _fsm = new int[statesCount, eventsCount];
        _currentState = default(TState);

        for (int i = 0; i < statesCount; ++i) {
            for (int j = 0; j < eventsCount; ++j)
                _fsm[i, j] = -1;
        }
    }


    public void SetRelation(TState sourceState, TEvent evt, TState destinyState) {
        int intSourceState = Convert.ToInt32(sourceState);
        int intEvt = Convert.ToInt32(evt);
        int intDestinyState = Convert.ToInt32(destinyState);

        _fsm[intSourceState, intEvt] = intDestinyState;
    }

    public void SetEvent(TEvent evt) {
        int currentState = Convert.ToInt32(_currentState);
        int toEvt = Convert.ToInt32(evt);

        if (_fsm[currentState, toEvt] != -1)
            _currentState = (TState)Enum.ToObject(typeof(TState), _fsm[currentState, toEvt]);

    }

    public TState GetState() {
        return _currentState;
    }
}