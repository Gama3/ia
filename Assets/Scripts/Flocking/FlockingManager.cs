﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flocking {
    public class FlockingManager : MonoBehaviour {

        /// <summary>
        /// An list for all boids 
        /// </summary>
        private List<Boid> boids;

        [SerializeField]
        private GameObject _boidPrefab = null;

        /// <summary>
        /// Amount of boids
        /// </summary>
        [SerializeField]
        private ushort _amountOfBoids = 300;

        /// <summary>
        /// Separation range
        /// </summary>
        [Range(0.1f, 100.0f)]
        public float _separationRange = 10;

        [Range(0.1f, 100.0f)]
        public float _neighbordist = 50;

        /// <summary>
        /// Maximum speed
        /// </summary>
        public float maxSpeed = 2f;

        /// <summary>
        /// Maximum steering force
        /// </summary>
        public float maxForce = 0.1f;

        [Range(0.1f, 100.0f)]
        public float cohesionWeight = 1.0f;

        [Range(0.1f, 100.0f)]
        public float separationWeight = 2.0f;

        [Range(0.1f, 100.0f)]
        public float alignmentWeight = 1.0f;

        private void Awake() {
            boids = new List<Boid>();

            for (int i = 0; i < _amountOfBoids; i++) {
                GameObject cowObject = GameObject.Instantiate<GameObject>(_boidPrefab);
                Boid cow = cowObject.GetComponent<Boid>();
                boids.Add(cow);
            }
        }

        public void Update() {
            for (int i = 0; i < boids.Count; i++)
                boids[i].UpdateMe(boids, _separationRange, _neighbordist,
                    maxSpeed, maxForce,
                    cohesionWeight, separationWeight, alignmentWeight);
        }

    }
}