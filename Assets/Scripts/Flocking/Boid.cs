﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flocking{
    public class Boid : MonoBehaviour {

        private Vector3 _velocity;
        private Vector3 _acceleration;

        private void Awake() {
            _acceleration = Vector3.zero;

            _velocity = new Vector3(Random.value * 100.0f, Random.value * 100.0f, Random.value * 100.0f);
        }

        private void ApplyForce(Vector3 force) {
            _acceleration += force;
        }

        private void UpdateLocation(float maxSpeed) {
            _velocity += _acceleration;

            _velocity = Limit(_velocity, maxSpeed);

            transform.position += _velocity;

            _acceleration *= 0;

            transform.rotation = Quaternion.FromToRotation(Vector3.forward, _velocity); 
        }


        private Vector3 Seek(Vector3 target, float maxSpeed, float maxForce) {
            Vector3 diff = target - transform.position; 

            diff.Normalize();
            diff *= maxSpeed;

            Vector3 steer = diff - _velocity;
            steer = Limit(steer, maxForce); 
            return steer;
        }

        private Vector3 Limit(Vector3 vector, float max) {
            if (vector.sqrMagnitude > max * max)
                return vector.normalized * max;
            else
                return vector;
        }

        private Vector3 Cohesion(List<Boid> boids, float neighbordist, float maxSpeed, float maxForce) {
            Vector3 sum = Vector3.zero;
            int count = 0;

            for (int i = 0; i < boids.Count; i++) {
                Boid current = boids[i];
                if (current == this)
                    continue;

                float d = Vector3.Distance(transform.position, current.transform.position);

                if ((d > 0) && (d < neighbordist)) {
                    sum += current.transform.position;
                    count++;
                }
            }

            if (count > 0) {
                sum /= count;

                return Seek(sum, maxSpeed, maxForce); 
            }

            return Vector3.zero;
        }

        private Vector3 Separation(List<Boid> boids, float separationRange, float maxSpeed, float maxForce) {
            Vector3 steer = Vector3.zero;
            int count = 0;

            for (int i = 0; i < boids.Count; i++) {
                Boid current = boids[i];
                if (current == this)
                    continue;

                Vector3 diff = transform.position - current.transform.position;
                float d = Vector3.SqrMagnitude(diff);

                if (d > 0 && d < separationRange) {
                    diff /= d;
                    steer += diff;
                    count++;      
                }
            }

            if (count > 0)
                steer /= (float)count;

            if (steer.sqrMagnitude > 0) {
                steer.Normalize();
                steer *= maxSpeed;
                steer -= _velocity;
                steer = Limit(steer, maxForce);
            }
            return steer;
        }

        private Vector3 Alignment(List<Boid> boids, float neighbordist, float maxSpeed, float maxForce) {
            Vector3 sum = Vector3.zero;
            int count = 0;

            for (int i = 0; i < boids.Count; i++) {
                Boid current = boids[i];

                float d = Vector3.Distance(transform.position, current.transform.position);
                if ((d > 0) && (d < neighbordist)) {
                    sum += current._velocity;
                    count++;
                }
            }

            if (count > 0) {
                sum /= (float)count;

                sum.Normalize();
                sum *= maxSpeed;
                Vector3 steer = sum - _velocity;
                steer = Limit(steer, maxForce);

                return steer;
            }

            return Vector3.zero;
        }


        public void UpdateMe(List<Boid> boids, float separationRange, float neighbordist,
            float maxSpeed, float maxForce,
            float cohesionWeight, float separationWeight, float alignmentWeight) {

            //Flock---------------------------------------------------------------------------------------
            Vector3 cohesion = Cohesion(boids, neighbordist, maxSpeed, maxForce);
            Vector3 separation = Separation(boids, separationRange, maxSpeed, maxForce);   
            Vector3 alignment = Alignment(boids, neighbordist, maxSpeed, maxForce);      

            cohesion *= cohesionWeight;
            separation *= separationWeight;
            alignment *= alignmentWeight;


            Vector3 result = cohesion + separation + alignment;
            ApplyForce(result);

            /*ApplyForce(cohesion);
            ApplyForce(separation);
            ApplyForce(alignment);*/

            //---------------------------------------------------------------------------------------

            UpdateLocation(maxSpeed);
        }

    }
}
