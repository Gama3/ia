﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BehaviourTree {
    /// <summary>
    /// A composite node is a node that can have one or more children.
    /// </summary>
    public abstract class CompositeNode : Node{
        protected List<Node> _childs;

        protected int _lastRunning;

        protected bool _rememberRunning;

        public CompositeNode(bool remenberRunning = false) {
            _childs = new List<Node>();
            _lastRunning = 0;
            _rememberRunning = remenberRunning;
        }

        public void AddChild(Node node) {
            _childs.Add(node);
        }

        public bool RemoveChild(Node node) {
            return _childs.Remove(node);
        }
    }
}
