﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BehaviourTree{
	public abstract class LogicNode : CompositeNode {

		protected LogicNode(bool loop) : base(loop){
		}
	}
}