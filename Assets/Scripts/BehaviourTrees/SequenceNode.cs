﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BehaviourTree{
    /// <summary>
    /// Sequence will visit each child in order, starting with the first,
    /// and when that succeeds will call the second, and so on down the list of children.
    /// If any child fails it will immediately return failure to the parent.
    /// If the last child in the sequence succeeds, then the sequence will return success to its parent.
    /// </summary>
	public class SequenceNode : CompositeNode {
		public SequenceNode(bool loop) : base(loop){}

		~SequenceNode(){}

		public override Status Execute (){
            int start = _rememberRunning ? _lastRunning : 0;

            for (int i = start; i < _childs.Count; i++) {
                Status status = _childs[i].Execute();
                if (status != Status.Success) {
                    _lastRunning = status == Status.InProcess ? i : 0;
                    return status;
                }
                else
                    _lastRunning = 0;
            }
            return Status.Success;
		}

	}


}
