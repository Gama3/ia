﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BehaviourTree{
	public class InverterNode : CompositeNode {

		protected InverterNode(bool loop) : base(loop){
		}

		public override Status Execute (){
			Node child = this._childs [0];

			Status s = child.Execute ();

			if (s == Status.Success)
				return Status.Fail;
			else if (s == Status.Fail)
				return Status.Success;
	
			//caso que no funciona aumentar variable aca this.index++;
			//if (++this._index == this._childs.Count)
			//	return Status.False;
			
			return s;

		}

	}
}
