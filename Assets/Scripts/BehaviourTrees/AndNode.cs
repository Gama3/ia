﻿
namespace BehaviourTree{
	public class AndNode : LogicNode {

		public AndNode(bool loop) : base(loop){}

		public override Status Execute (){
			if (this._childs.Count < 2)
				return Status.Fail;

			Status s1 = this._childs [0].Execute ();
			Status s2 = this._childs [1].Execute ();

			return s1 == Status.Success && s2 == Status.Success ? Status.Success : Status.Fail;
		}
	}
}
