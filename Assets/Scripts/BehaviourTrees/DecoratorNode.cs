﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BehaviourTree {
    public abstract class DecoratorNode : Node{
        
        private Node _child;

        protected DecoratorNode() {
            _child = null;
        }

        public void SetChild(Node child) {
            _child = child;
        }

        public Node GetChild() {
            return _child;
        }
    }
}
