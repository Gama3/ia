﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BehaviourTree{
	/// <summary>
    /// Selector will return a success if any of its children succeed and not process any further children.
    /// It will process the first child, and if it fails will process the second,
    /// and if that fails will process the third, until a success is reached
	/// </summary>
    public class SelectorNode : CompositeNode {

		public SelectorNode(bool loop) : base(loop){}

		public override Status Execute (){
            int start = _rememberRunning ? _lastRunning : 0;
            for (int i = start; i < _childs.Count; i++) {
                Node node = _childs[i];
                Status status = node.Execute();
                if (status != Status.Fail) {
                    _lastRunning = status == Status.InProcess ? i : 0;
                    return status;
                }
            }
            return Status.Fail;
		}

	}
}
