﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BehaviourTree{
	public enum Status{
		None = 0,
		Success,
		Fail,
		InProcess
	}

	public abstract class Node {

        protected Status _currentStatus;

        protected Status _lastStatus;

        protected Node(){
            _currentStatus = Status.None;
            _lastStatus = Status.None;
        }

		protected virtual bool Validate (){ //abstract
			return true;
		}

        protected virtual void Awake() { } //abstract

        protected virtual void Sleep() { } //abstract

        public virtual void Reset() { }

		public abstract Status Execute (); //abstract //Update

        public Status GetStatus(){
            return _currentStatus;
        }
        /*public Status Execute (){
			if (Validate ()) {
				if (_currentStatus != Status.InProcess) {
					Awake ();
					_currentStatus = Status.InProcess;
				}

				_lastStatus = _currentStatus = Update ();

				if(_currentStatus != Status.InProcess)
					Reset();
			}
			else {
                _lastStatus = Status.Fail;
				Reset ();
			}
			return _lastStatus;
		}*/


	}
}
