﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PathFinder{
	public class BreadthFirst : PathFinder {

		private Queue<Node> _openNodes;

		private Queue<Node> _closedNodes;

		public BreadthFirst(){
			_openNodes = new Queue<Node> ();
			_closedNodes = new Queue<Node> ();
		}

        private void OpenNode(Node node) {
            node.visited = true;
           _openNodes.Enqueue(node);
        }

        private void CloseNode(Node node) {
            _closedNodes.Enqueue(node);
        }

        protected override Node SelectNode() {
            Node node = _openNodes.Dequeue();
            for(int i = 0; i < node.adjacent.Count; ++i) {
                Node n = node.adjacent[i];

                if(n.isObstacle)
                    continue;

                if(!n.visited) {
                    OpenNode(n);
                    n.parent = node;
                }
            }

            return node;//_openNodes.Dequeue();
        }

        public override List<Node> GetPath(Node startNode, Node endNode) {
            _openNodes.Clear();
            _closedNodes.Clear();

            //Reset every Node on the grid
            Reset();

            OpenNode(startNode);

            while(_openNodes.Count > 0) {
                Node node = SelectNode();

                //si node es destino(endNode)
                if(node == endNode)
                    return GetPath(startNode, endNode, node);

                CloseNode(node);

                /*for(int i = 0; i < node.adjacent.Count; ++i) {
                    Node n = node.adjacent[i];

                    if(n.isObstacle) 
                        continue;

                    if (!n.visited){
                        OpenNode(n);
						n.parent = node;
					}
                }*/

            }
            return null;
        }



	}

}