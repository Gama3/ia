﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PathFinder{
	public class DepthFirst : PathFinder {

		private Stack<Node> _openNodes;

		private Queue<Node> _closedNodes;

		public DepthFirst(){
			_openNodes = new Stack<Node> ();
			_closedNodes = new Queue<Node> ();
		}

        protected override Node SelectNode() {
            Node node = _openNodes.Pop();
            for(int i = 0; i < node.adjacent.Count; ++i) {
                Node n = node.adjacent[i];

                if(n.isObstacle)
                    continue;

                if(!n.visited) {
                    OpenNode(n);
                    n.parent = node;
                }
            }

            return node;
        }

        public void OpenNode(Node node) {
            node.visited = true;
            this._openNodes.Push(node);
        }

        public void CloseNode(Node node) {
            this._closedNodes.Enqueue(node);
        }

        public override List<Node> GetPath (Node startNode, Node endNode){
			_openNodes.Clear ();
			_closedNodes.Clear ();

            //Reset every Node on the grid
            Reset();

			OpenNode (startNode);
            
			while (_openNodes.Count > 0) {
				Node node = SelectNode ();

				if (node == endNode) 
                    return GetPath(startNode, endNode, node);

				CloseNode (node);

				/*for (int i = 0; i < node.adjacent.Count; ++i) {
					Node n = node.adjacent [i];

                    if(n.isObstacle)
                        continue;

                    if(!n.visited){
						OpenNode (n);
						n.parent = node;
					}
				}*/
			}
			return null;
		}

	}

}