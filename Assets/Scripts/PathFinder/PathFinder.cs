﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PathFinder{
	public abstract class PathFinder {

		private static PathFinder _sInstance = null;

		protected Node[,] _grid;

		public enum FindMode{
			WithDiagonals = 0,
			WithoutDiagonals
		}

        public bool debug {
            get;
            set;
        }

        public FindMode findMode {
            get;
            set;
        }

        public PathFinderAlgorithm pathFinderAlgorithm {
            get;
            set;
        }

		protected PathFinder () {
			_sInstance = this;
			_grid = null;
            debug = false;
            findMode = FindMode.WithoutDiagonals;
            pathFinderAlgorithm = PathFinderAlgorithm.BreadthFirts;
		}

        //returns the reverse Path
        protected List<Node> GetPath(Node startNode, Node endNode, Node node) {
            List<Node> path = new List<Node>();
            
            //aniado el ultimo nodo -> endNode
            path.Add(node);

            //debug only
            if(debug) {
                startNode.color = Color.green;
                endNode.color = Color.red;
            }
            //
            while (node.parent != null) {
                //debug only
                if (debug && node.parent.color != Color.red && node.parent.color != Color.green)
                    node.parent.color = Color.yellow;
                //

                path.Add(node.parent);
                node.parent = node.parent.parent;
            }
            path.Reverse();

            return path;
        }

        protected abstract Node SelectNode();

        protected void GetAdjacentNodes(Node node) {
            int x = 0;
            int z = 0;
            NodeFromMatrix(node, out x, out z);

            switch(findMode) {
                case FindMode.WithDiagonals:
                    for(int ix = x - 1; ix <= x + 1; ++ix) {
                        for(int iz = z - 1; iz <= z + 1; ++iz) {
                            if(IsInGridRange(ix, iz) && ix != x && iz != z)
                                node.adjacent.Add(_grid[ix, iz]);
                        }
                    }
                    break;
                case FindMode.WithoutDiagonals:
                    if(IsInGridRange(x + 1, z)) {
                        Node right = _grid[x + 1, z];
                        node.adjacent.Add(right);
                    }

                    if(IsInGridRange(x - 1, z)) {
                        Node left = _grid[x - 1, z];
                        node.adjacent.Add(left);
                    }

                    if(IsInGridRange(x, z + 1)) {
                        Node forward = _grid[x, z + 1];
                        node.adjacent.Add(forward);
                    }

                    if(IsInGridRange(x, z - 1)) {
                        Node backward = _grid[x, z - 1];
                        node.adjacent.Add(backward);
                    }

                    break;
            }
        }


        protected float GetManhattanDistance(Node start, Node end) {
            int startX = 0;
            int startY = 0;
            NodeFromMatrix(start, out startX, out startY);

            int endX = 0;
            int endY = 0;
            NodeFromMatrix(end, out endX, out endY);

            int resultX = startX;
            while(resultX < endX)
                ++resultX;

            int resultY = startY;
            while(resultY < endY)
                ++resultY;

            return (float)resultX + (float)resultY;
        }

        protected float Sqrt(Node startNode, Node destinyNode) {
            int currentX = 0;
            int currentZ = 0;
            NodeFromMatrix(startNode, out currentX, out currentZ);

            int targetX = 0;
            int targetZ = 0;
            NodeFromMatrix(destinyNode, out targetX, out targetZ);

            return Mathf.Sqrt(Mathf.Pow(destinyNode.transform.position.x - startNode.transform.position.x, 2.0f)
                + Mathf.Pow(destinyNode.transform.position.y - startNode.transform.position.y, 2.0f)
                + Mathf.Pow(destinyNode.transform.position.z - startNode.transform.position.z, 2.0f));
        }

        protected float GetDistance(Node current, Node target) {
            if(findMode == FindMode.WithoutDiagonals)
                return GetManhattanDistance(current, target);

            return Sqrt(current, target);
        }

        public static PathFinder GetInstance(){
			return _sInstance;
		}
			
		public void GenerateGrid(Transform parent, int x, int z){
			_grid = new Node[x, z];

			GameObject go = Manager.PrefabManager.GetInstance ()._nodePrefab;
			int value = 0;
			for (int xx = 0; xx < x; ++xx) {
				for (int zz = 0; zz < z; ++zz) {
					GameObject currentGo = GameObject.Instantiate (go,
						                       new Vector3 (zz + go.transform.lossyScale.y, 0.0f, -(xx + go.transform.lossyScale.x)),
						                       Quaternion.identity);

					Node currentNode = currentGo.GetComponent<Node> ();
                    if(debug)
					    currentGo.name = string.Format ("Node[{0},{1}]", xx, zz);

					currentGo.transform.SetParent (parent);

					_grid [xx, zz] = currentNode;
					++value;
				}
			}
		}

		//calls Reset on every node of this grid
		public void Reset(bool clearAdjancent = false) {
			for (int i = 0; i < _grid.GetLength(0); ++i) {
				for (int j = 0; j < _grid.GetLength(1); ++j)
					_grid[i, j].Reset(clearAdjancent);
			}
		}

		public void GetAdjacent() {
			for (int xx = 0; xx < _grid.GetLength(0); ++xx) {
				for (int zz = 0; zz < _grid.GetLength(1); ++zz)
					GetAdjacentNodes(_grid[xx, zz]);
			}
		}

		public bool IsInGridRange(int x, int z) {
			return x >= 0 && x < _grid.GetLength(0) && z >= 0 && z < _grid.GetLength(1);
		}

        public Node GetNodeAt(int x, int z) {
            return IsInGridRange(x, z) ? _grid[x, z] : null;
        }

		public bool NodeFromMatrix (Node node, out int x, out int z){
			for (int ix = 0; ix < _grid.GetLength (0); ++ix) {
				for (int iz = 0; iz < _grid.GetLength (1); ++iz) {
					if (_grid [ix, iz] == node){
						x = ix;
						z = iz;
						return true;
					}
				}
			}
			x = -1;
			z = -1;
            return false;
		}

        public abstract List<Node> GetPath (Node startNode, Node endNode);
	}

}