﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PathFinder{
	public class Node : MonoBehaviour {
		[SerializeField]
		private MeshRenderer _renderer = null;

		public Node parent {
            get;
            set;
        }

		public List<Node> adjacent {
            get;
            set;
        }

		public bool isObstacle {
            get;
            set;
        }

        public bool visited {
            get;
            set;
        }

		//Heuristic, h cost.
		public float hCost {
            get;
            set;
        }

		// g(x) is the movement cost to move from the starting point to the given square using the path generated to get there.
        //cost a moverse desde la posicion actual a los adyacentes
		public float gCost {
            get;
            set;
        }

        public Color color {
            get {
                return _renderer.material.color;
            }
            set {
                _renderer.material.color = value;
            }
        }

		private void Awake(){
            Reset(true);
		}

		private void OnMouseDown(){
			int x = 0;
			int y = 0;
			PathFinder.GetInstance ().NodeFromMatrix (this, out x, out y);
            if (DebugOnly.UIDebug.GetInstance() == null)
                return;
			DebugOnly.UIDebug.GetInstance ().SetPosMatrixText (string.Format ("Matrix: [{0},{1}]", x, y));

			DebugOnly.UIDebug.GetInstance ().SetCostText ("Cost: " + gCost.ToString());
			DebugOnly.UIDebug.GetInstance ().SetHeuristicText ("Heuristic: " + hCost.ToString ());
		}

		public void Reset(bool clearAdjacent) {
            if (clearAdjacent) {
                if (adjacent == null)
                    adjacent = new List<Node>();

                adjacent.Clear();
            }
			parent = null;
			gCost = 0;
			hCost = 0.0f;
            visited = false;

            if(color != Color.black)
                color = Color.white;
		}

		//f(x) = g(x) + h(x)
		public float GetFCost(){
			return hCost + gCost;
		}
	}

}