﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PathFinder{
//theta*
	public class AStar : PathFinder {
        private List<Node> _openNodes;
        private List<Node> _closedNodes;

        public AStar(){
            _openNodes = new List<Node>();
            _closedNodes = new List<Node>();
        }

		~AStar(){}

		private void FindBestNode (Node node){
            /*for (int i = 0; i < currentNode.adjacent.Count; ++i) {
				float cost = currentNode.GetGCost () + CalculateGcost (currentNode, currentNode.adjacent [i]);

				if (_openNodes.Contains (currentNode.adjacent [i]) && cost < currentNode.adjacent [i].GetGCost ())
					_openNodes.Remove (currentNode.adjacent [i]);

				if (_closedNodes.Contains (currentNode.adjacent [i]) && cost < currentNode.adjacent [i].GetGCost ())
					_closedNodes.Remove (currentNode.adjacent [i]);

				if(!_openNodes.Contains(currentNode.adjacent[i]) && !_closedNodes.Contains(currentNode.adjacent[i])){
					currentNode.adjacent [i].SetGCost ((int)cost);
					currentNode.adjacent [i].SetHCost (currentNode.adjacent [i].GetGCost () + currentNode.adjacent [i].GetHCost ());
					currentNode.adjacent [i].parent = currentNode;
					_openNodes.Add (currentNode.adjacent [i]);
				}
			}*/

            for(int i = 0; i < node.adjacent.Count; ++i) {
                Node current = node.adjacent[i];

                if(current.isObstacle || _closedNodes.Contains(current))
                    continue;

                float newCost = node.gCost + GetDistance(node, current);

                if(newCost < current.gCost || !_openNodes.Contains(current)) {
                    current.gCost = newCost;
                    current.parent = node;

                    if(!_openNodes.Contains(current))
                        _openNodes.Add(current);
                }

            }
        }

        private void OpenNode(Node node) {
            node.visited = true;
            _openNodes.Add(node);
        }

        protected override Node SelectNode() {
            Node currentNode = _openNodes[0];
            _openNodes.RemoveAt(0);

            _closedNodes.Add(currentNode);

            return currentNode;
        }

        public override List<Node> GetPath (Node startNode, Node endNode){
            _openNodes.Clear();
            _closedNodes.Clear();

            Reset ();

            OpenNode(startNode);

            while(_openNodes.Count > 0) {
                Node currentNode = SelectNode();

                if(currentNode == endNode)
                    return GetPath(startNode, endNode, currentNode);

				FindBestNode(currentNode);
            }
			return null;
		}



	}

}