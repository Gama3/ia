﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PathFinder{
	public class Dijkstra : PathFinder {

		private List<Node> _openNodes;
		private List<Node> _closedNodes;

		public Dijkstra(){
			_openNodes = new List<Node> ();
			_closedNodes = new List<Node> ();
		}


        private void FindBestNode(Node node) {
            for(int i = 0; i < node.adjacent.Count; ++i) {
                Node current = node.adjacent[i];

                if(current.isObstacle || _closedNodes.Contains(current))
                    continue;

                float newCost = node.gCost + GetDistance(node, current);

                if(newCost < current.gCost || !_openNodes.Contains(current)) {
                    current.gCost = newCost;
                    current.parent = node;

                    if(!_openNodes.Contains(current))
                        _openNodes.Add(current);
                }

            }
        }

        private void OpenNode(Node node) {
            node.visited = true;
            _openNodes.Add(node);
        }

        protected override Node SelectNode() {
            Node n = _openNodes[0];
            _openNodes.RemoveAt(0);

            _closedNodes.Add(n);
            return n;
        }

        public override List<Node> GetPath (Node startNode, Node endNode){
			_openNodes.Clear ();
			_closedNodes.Clear ();

			Reset();

            OpenNode(startNode);

            while (_openNodes.Count > 0) {
                Node currentNode = SelectNode();

				if (currentNode == endNode) 
                    return GetPath(startNode, endNode, currentNode);
				
				FindBestNode (currentNode);
			}
			return null;
		}
	}

}